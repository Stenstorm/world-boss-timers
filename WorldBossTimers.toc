## Interface: 70000
## Title: WorldBossTimers
## Notes: Creates a timer for world boss kills
## Author: Soulstorm
## Version: 0.1
## SavedVariables: WorldBossTimersDB
embeds.xml

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceConsole-3.0\AceConsole-3.0.xml
Libs\AceDB-3.0\AceDB-3.0.xml
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml

WorldBossTimers.lua
